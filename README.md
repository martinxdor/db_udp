 # Curso de Base de Datos

 ## ¿ Como descargar el contenido ?
Hay dos formas
1.  Desde la web del proyecto en gitlab 
[Botón de Descarga](/resources/imgs/download.png "Descarga")

2. Por consola
```bash
git clone https://gitlab.com/l30bravo/db_udp.git
```
## Programa del Curso
* [Programa del Curso de Base de Datos 2022](/resources/pdf/programa_curso_base_de_datos.pdf "Programa del Curso")
* [Conograma del Curso](/resources/planificacion_sem_1_2022.ods "Cronograma del Curso") - Actualizado `2022-05-03`

## Proyecto
* Pauta de Evaluación [ [ODP](/resources/proyecto/evaluaci%C3%B3n.odt) | [PDF](/resources/proyecto/evaluaci%C3%B3n.pdf ) ]
* Pre-Evaluación - `Desde el 14 Marzo` [Link](https://forms.gle/grq4kQK1VL3axYtW7) (Entrar con el correo de la UDP)
* Avance 1  - `8 de Abril`  [ [ODP](/resources/proyecto/templata_entrega_1.odt) | [PDF](/resources/proyecto/templata_entrega_1.pdf ) ]
* Avance 2 - `Semana del 2 de Mayo` [ [ODP](/resources/proyecto/templata_entrega_2.odt) | [PDF](/resources/proyecto/templata_entrega_2.pdf) ]
* Presentación Final - `Semana del 30 de Mayo` [ [ODP](/resources/proyecto/Presentacion.odt) | [PDF](/resources/proyecto/Presentacion.pdf) ] 

## Ayudantias
Proximamente...

## Clases
1. Introducción [ [ODP](/resources/clases/bdd_1_%20Introducci%C3%B3n.odp) | [PDF](/resources/clases/bdd_1_%20Introducci%C3%B3n.pdf ) ]
2. Características generales de las BDD [ [ODP](/resources/clases/bdd_2_%20Caracter%C3%ADsticas%20generales%20de%20las%20BDD.odp) | [PDF](/resources/clases/bdd_2_%20Caracter%C3%ADsticas%20generales%20de%20las%20BDD.pdf) ]
3. Modelamiento de BDD - Parte 1 [ [ODP](/resources/clases/bdd_3_%20modelamiento_base_de_datos_parte1.odp) | [PDF](/resources/clases/bdd_3_%20modelamiento_base_de_datos_parte1.pdf) ]
4. Modelamiento de BDD - Parte 2 [ [ODP](/resources/clases/bdd_4_%20modelamiento_base_de_datos_parte2.odp) | [PDF](/resources/clases/bdd_4_%20modelamiento_base_de_datos_parte2.pdf) ]
4. Modelamiento de BDD - Parte 3 [ [ODP](/resources/clases/bdd_5_%20modelamiento_base_de_datos_parte3.odp) | [PDF](/resources/clases/bdd_5_%20modelamiento_base_de_datos_parte3.pdf) ]
5. Modelamiento de BDD - Parte 4 - Ejercicios [ [ODP](/resources/clases/bdd_6_Ejercicios.odp) | [PDF](/resources/clases/bdd_6_Ejercicios.pdf) ]
6. Modelamiento de BDD - De ER a SQL parte I  [ [ODP](/resources/clases/bdd_7_de_er_a_sql_parte_1.odp) | [PDF](/resources/clases/bdd_7_de_er_a_sql_parte_1.pdf) ]
7. Modelamiento de BDD - De ER a SQL parte II + Agebra Relacional Parte I - SELECT  [ [ODP](/resources/clases/bdd_8_de_er_a_sql_parte_2.odp) | [PDF](/resources/clases/bdd_8_de_er_a_sql_parte_2.pdf) ]
8. Algebra relacional y SQL parte II - SELECT  [ [ODP](/resources/clases/bdd_9_algebra_relacional_y_sql_part_ii.odp) | [PDF](/resources/clases/bdd_9_algebra_relacional_y_sql_part_ii.pdf) ]
9.  Algebra relacional y SQL parte III - SELECT  [ [ODP](/resources/clases/bdd_10_algebra_relacional_y_sql_part_iii.odp) | [PDF](/resources/clases/bdd_10_algebra_relacional_y_sql_part_iii.pdf) ] 
10.  SQL parte IV - Teorema de Mishi  [ [ODP](/resources/clases/bdd_11_algebra_relacional_y_sql_part_iv.odp) | [PDF](/resources/clases/bdd_11_algebra_relacional_y_sql_part_iv.pdf) ] 
11. SQL parte V - Tecnica de la Fusión  [ [ODP](/resources/clases/bdd_12_algebra_relacional_y_sql_part_v.odp) | [PDF](/resources/clases/bdd_12_algebra_relacional_y_sql_part_v.pdf) ] 
13. Entrenamiento Solemne 1  [ [ZIP](/resources/clases/bdd_13_repaso.zip) 
14. Pruebas Pasadas - Solemne 1  [ [ZIP](/resources/clases/PruebasPasadas.zip)
15. Pauta Solemne 1  [[ODP](/resources/clases/bdd_14_pauta_solemne_1.odp) | [ZIP](/resources/clases/sole1.zip)


## Apuntes
* [¿Como instalar Postgres?](postgres_install.md)
* [Actualizar password de Postgres](postgres_update_pass.md)
* [¿Como instalar PGModeler?](https://gitlab.com/l30bravo/db_udp/-/blob/main/resources/scripts/pg_modeler_install.sh)


## Tools
* [Draw.io](https://github.com/jgraph/drawio-desktop) - Software para modelar
* [PGModeler](https://github.com/pgmodeler/pgmodeler) - UI para modelar y consultar DBs Postgre (crea el modelo de datos e implemta el modelo en una DB Postgre)
* [DBeaver](https://dbeaver.io/) - UI para consultar DBs (PostgreSQL, MySQL, SQLite, Oracle, etc)

## Bibliografia
* [Introducción a los sistemas de Base de Datos](/resources/pdf/Introduccion%20a%20los%20Sistemas%20de%20Bases%20de%20Datos%20-%207ma%20Edicion%20-%20C.%20J.%20Date.pdf "Introducción a los sistemas de Base de Datos")

* [Manual de PostgreSQL](/resources/pdf/Postgres-User.pdf "Manual de PostgreSQL")
